var express = require('express');
var router = express.Router();

var jobs = [
  {
    company: 'craftworkz',
    title: 'Senior Java Developer',
    description: 'Esse inimicus cotidieque in duo. Id adhuc ullum commune has. Vim quis philosophia signiferumque at, eum id regione deterruisset signiferumque, sea legere viderer eu. Ea posse tritani iuvaret qui, sale dicta concludaturque ea vim. Eos cu mutat propriae, has at fugit homero graeci.Nisl homero praesent vix cu.',
    date: '15 june 2016',
    location: 'Leuven'
  },
  {
    company: 'ibizz',
    title: 'Senior Java Developer',
    description: 'Esse inimicus cotidieque in duo. Id adhuc ullum commune has. Vim quis philosophia signiferumque at, eum id regione deterruisset signiferumque, sea legere viderer eu. Ea posse tritani iuvaret qui, sale dicta concludaturque ea vim. Eos cu mutat propriae, has at fugit homero graeci.Nisl homero praesent vix cu.',
    date: '30 May 2016',
    location: 'Kontich'
  },
  {
    company: 'optis',
    title: 'Senior Java Developer',
    description: 'Esse inimicus cotidieque in duo. Id adhuc ullum commune has. Vim quis philosophia signiferumque at, eum id regione deterruisset signiferumque, sea legere viderer eu. Ea posse tritani iuvaret qui, sale dicta concludaturque ea vim. Eos cu mutat propriae, has at fugit homero graeci.Nisl homero praesent vix cu.',
    date: '19 May 2016',
    location: 'Kontich'
  },
  {
    company: 'optis',
    title: 'Senior Java Developer',
    description: 'Esse inimicus cotidieque in duo. Id adhuc ullum commune has. Vim quis philosophia signiferumque at, eum id regione deterruisset signiferumque, sea legere viderer eu. Ea posse tritani iuvaret qui, sale dicta concludaturque ea vim. Eos cu mutat propriae, has at fugit homero graeci.Nisl homero praesent vix cu.',
    date: '19 May 2016',
    location: 'Kontich'
  }
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', 
    { 
      title: 'Jobbox - ICT Jobs Vlaanderen', 
      jobs: jobs, 
      header: 'IT Jobs in Vlaanderen', 
      subHeader: 'Op zoek naar een nieuwe uitdaging...' 
    });
});

router.get('/detail/:id', function(req, res, next) {
  res.render('detail', 
    { 
      header: jobs[req.params.id].title,  
      subHeader: jobs[req.params.id].company,
      location: jobs[req.params.id].location
    });
});

module.exports = router;
