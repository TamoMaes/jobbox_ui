$(document).ready(function(){

    // window height
    var window_height = $(window).height();
    $('.masthead.segment').height(window_height);

    // init animations
    new WOW().init();

    // init moment
    moment.locale('nl');
    $('.date.label').each(function(el){
        var timeAgo = moment($(this).data('date')).fromNow();
        $(this).append(timeAgo);
    });

    $('.date.label').popup();
});